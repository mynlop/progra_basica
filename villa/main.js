var mv = document.getElementById("miVilla");
var papel = mv.getContext("2d");

var fondo = {
    url: "tile.png",
    cargaOK: false
};
var vaca = {
    url: "vaca.png",
    cargaOK: false
};
var pollo = {
    url: "pollo.png",
    cargaOK: false
};
var cerdo = {
    url: "cerdo.png",
    cargaOK: false
};

fondo.imagen = new Image();
fondo.imagen.src = fondo.url;
fondo.imagen.addEventListener("load",cargarFondo);
vaca.imagen = new Image();
vaca.imagen.src = vaca.url;
vaca.imagen.addEventListener("load", cargarVaca);
pollo.imagen = new Image();
pollo.imagen.src = "pollo.png";
pollo.imagen.addEventListener("load",cargarPollo);
cerdo.imagen = new Image();
cerdo.imagen.src = "cerdo.png";
cerdo.imagen.addEventListener("load",cargarCerdo);

function cargarFondo(){
    fondo.cargaOK = true;
    dibujar();
}
function cargarVaca(){
    vaca.cargaOK = true;
    dibujar();
}
function cargarPollo(){
    pollo.cargaOK = true;
    dibujar();
}
function cargarCerdo(){
    cerdo.cargaOK = true;
    dibujar();
}
var cantidad = aleatorio(0,15);
function dibujar(){
    if(fondo.cargaOK){
        papel.drawImage(fondo.imagen, 0, 0 );
    }
    if(vaca.cargaOK){
        console.log(cantidad);
        for(var i = 0; i < cantidad; i++){
            var x = aleatorio(0,6);
            var y = aleatorio(0,6);
            x = x * 70;
            y = y * 70;
            papel.drawImage(vaca.imagen, x, y );
        }
    }
    if(pollo.cargaOK){
        for(var i = 0; i < cantidad; i++){
            var x = aleatorio(0,6);
            var y = aleatorio(0,6);
            x = x * 70;
            y = y * 70;
            papel.drawImage(pollo.imagen, x, y );
        }
    }
    if(cerdo.cargaOK){
        for(var i = 0; i < cantidad; i++){
            var x = aleatorio(0,6);
            var y = aleatorio(0,6);
            x = x * 70;
            y = y * 70;
            papel.drawImage(cerdo.imagen, x, y );
        }
    }
}

function aleatorio(min, maxi){
    var resultado =  Math.floor(Math.random() * (maxi - min + 1)) + min;
    return resultado;
}