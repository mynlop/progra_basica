var mv = document.getElementById("miVilla");
var papel = mv.getContext("2d");
var teclas = {
    UP: 38,
    DOWN: 40,
    LEFT: 37,
    RIGHT: 39
};
document.addEventListener("keyup", dibujar);
var fondo = {
    url: "tile.png",
    cargaOK: false
};
var cerdo = {
    url: "cerdo.png",
    cargaOK: false
};
var vaca = {
    url: "vaca.png",
    cargaOK: false
};

fondo.imagen = new Image();
fondo.imagen.src = fondo.url;
fondo.imagen.addEventListener("load",cargarFondo);
cerdo.imagen = new Image();
cerdo.imagen.src = cerdo.url;
cerdo.imagen.addEventListener("load",cargarCerdo);
vaca.imagen = new Image();
vaca.imagen.src = vaca.url;
vaca.imagen.addEventListener("load", cargarVaca);

function cargarFondo(){
    fondo.cargaOK = true;
    dibujarDos();
}
function cargarCerdo(){
    cerdo.cargaOK = true;
    dibujarDos();
}
function cargarVaca(){
    vaca.cargaOK = true;
    dibujarDos();
}
function dibujarDos(){
    if(fondo.cargaOK){
        papel.drawImage(fondo.imagen, 0, 0 );
    }
    // if(cerdo.cargaOK){
    //     posicion(x,y);
    // }
    if(vaca.cargaOK){
        console.log(cantidad);
        for(var i = 0; i < cantidad; i++){
            var x = aleatorio(0,6);
            var y = aleatorio(0,6);
            x = x * 70;
            y = y * 70;
            papel.drawImage(vaca.imagen, x, y );
        }
    }
}
function posicion(a,b){
    papel.drawImage(cerdo.imagen, a, b );    
}
function aleatorio(min, maxi){
    var resultado =  Math.floor(Math.random() * (maxi - min + 1)) + min;
    return resultado;
}
var x = aleatorio(0,6);
var y = aleatorio(0,6);
x = x * 70;
y = y * 70;
var movimiento = 30;
var cantidad = aleatorio(0,15);

function dibujar(evento){
    switch(evento.keyCode){
        case teclas.UP:
            console.log("Arriba");
            papel.drawImage(cerdo.imagen, x, (y - movimiento) );
            y = y - movimiento;
            dibujarDos();
            posicion(x, y);
        break;
        case teclas.DOWN:
            console.log("Abajo");
            papel.drawImage(cerdo.imagen, x, (y + movimiento) );
            y = y + movimiento;
            dibujarDos();
            posicion(x, y);
        break;
        case teclas.LEFT:
            console.log("Izquierda");
            papel.drawImage(cerdo.imagen, (x - movimiento), y );
            x = x - movimiento;
            dibujarDos();
            posicion(x, y);
        break;
        case teclas.RIGHT:
            console.log("Derecha");
            papel.drawImage(cerdo.imagen, (x + movimiento), y );
            x = x + movimiento;
            dibujarDos();
            posicion(x, y);
        break;
        default:
            console.log("Otra tecla");
        break;
    }
}