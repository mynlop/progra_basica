class Billete{
    constructor(v, c){
        this.valor = v;
        this.cantidad = c;
    }
}

var caja = [];
var entregado = [];

caja.push(new Billete(100,5));
caja.push(new Billete(50,10));
caja.push(new Billete(20,5));
caja.push(new Billete(10,10));
caja.push(new Billete(5,5));

var dinero = 0;
var resDiv = 0;
var billetes = 0;

var resultado =  document.getElementById("resultado");
var b = document.getElementById("extraer");
b.addEventListener("click", entregarDinero);

function entregarDinero(){
    var t = document.getElementById("dinero");
    dinero = parseInt(t.value);
    console.log(dinero);
    for(var bi of caja){
        if(dinero > 0){
            resDiv = Math.floor(dinero / bi.valor);
            if(resDiv > resDiv.cantidad){
                billetes = bi.cantidad;
            }else{
                billetes = resDiv;
            }
            entregado.push(new Billete(bi.valor, billetes));
            dinero = dinero - (bi.valor * billetes);
        }
    }
}