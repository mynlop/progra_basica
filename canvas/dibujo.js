var d = document.getElementById("dibujito");
var lienzo = d.getContext("2d");
var canvasAncho = d.width;
var cntLineas = document.getElementById("txtcntLineas"); 
var boton = document.getElementById("btnBoton");
console.log(canvasAncho);
boton.addEventListener("click",dibujar);

function dibujar(){
    var lineas = 30;
    var l2 = 310;
    var color = "red";
    var valorLineas = parseInt(cntLineas.value);
    var espacio = canvasAncho / valorLineas;
    for(l = 0; l < lineas; l++){
        inicio = espacio * l;
        fin = espacio * (l + 1);
        // l2 = l2 - 10;
        inicioDos = (l2 -= espacio) * 1;
        dibujarLinea(color,0,inicio,fin,300);
        dibujarLinea("blue",300,inicio,fin,0);
        dibujarLinea("green",0,inicioDos,fin,0);
        dibujarLinea("yellow",300,inicioDos,fin,300);
        // console.log("xi: " + inicio + "|| yf: " + fin);
        // console.log(inicioDos + " ||" + fin);
        // console.log("Lineas: " + l);
        // console.log(l2);
    }
    // dibujarLinea(color,1,1,1,299);
    // dibujarLinea(color,1,299,299,299);
    // dibujarLinea("blue",1,1,299,1);
    // dibujarLinea("blue",299,1,299,299);
}
function dibujarLinea(color, xInicial, yInicial, xFinal, yFinal){
    lienzo.beginPath();
    lienzo.strokeStyle = color;
    lienzo.moveTo(xInicial, yInicial);
    lienzo.lineTo(xFinal, yFinal);
    lienzo.stroke();
    lienzo.closePath();
}