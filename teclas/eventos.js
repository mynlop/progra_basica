var teclas = {
    UP: 38,
    DOWN: 40,
    LEFT: 37,
    RIGHT: 39
};

document.addEventListener("keyup", dibujar);
document.addEventListener("mousedown", presionarMouse);
document.addEventListener("mouseup", soltarMouse);
document.addEventListener("mousemove", dibujarMouse);


var d = document.getElementById("miCanvas");
var papel = d.getContext("2d");
var x = 150;
var y = 150;
var estado = 0;
var colorcito = "blue";
var movimiento = 10;

dibujarLinea("blue",x-1,y-1,x+1,x+1,papel);

function presionarMouse(evento){
    estado = 1;
    x = evento.layerX;
    y = evento.layerY;
}
function soltarMouse(evento){
    estado = 0;
    x = evento.layerX;
    y = evento.layerY;
}
function dibujarMouse(evento){
    if(estado == 1){
        dibujarLinea(colorcito, x, y, evento.layerX, evento.layerY, papel);
    }
    x = evento.layerX;
    y = evento.layerY;
}

function dibujarLinea(color, xInicial, yInicial, xFinal, yFinal, lienzo){
    lienzo.beginPath();
    lienzo.strokeStyle = color;
    lienzo.lineWidth = 3;
    lienzo.moveTo(xInicial, yInicial);
    lienzo.lineTo(xFinal, yFinal);
    lienzo.stroke();
    lienzo.closePath();
}

function dibujar(evento){
    switch(evento.keyCode){
        case teclas.UP:
            console.log("Arriba");
            dibujarLinea(colorcito, x, y, x, (y - movimiento), papel);
            y = y - movimiento;
        break;
        case teclas.DOWN:
            console.log("Abajo");
            dibujarLinea(colorcito, x, y, x, (y + movimiento), papel);
            y = y + movimiento;
        break;
        case teclas.LEFT:
            console.log("Izquierda");
            dibujarLinea(colorcito, x, y, (x - movimiento), y, papel);
            x = x - movimiento;
        break;
        case teclas.RIGHT:
            console.log("Derecha");
            dibujarLinea(colorcito, x, y, (x + movimiento), y, papel);
            x = x + movimiento;
        break;
        default:
            console.log("Otra tecla");
        break;
    }
}